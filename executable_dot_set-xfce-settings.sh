#!/bin/bash

set -eux -o pipefail

xfconf-query -c xsettings -p /Net/ThemeName -s "Arc-Dark"
xfconf-query -c xsettings -p /Net/IconThemeName -s "Paper"
xfconf-query -c xsettings -p /Gtk/FontName -s "Source Sans Pro Regular 11"
xfconf-query -c keyboards -p /Default/KeyRepeat/Delay 250
xfconf-query -c keyboards -p /Default/KeyRepeat/Rate 45
