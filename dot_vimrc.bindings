" Define leader key to be <Space>
let g:mapleader=' '

" Tab navigation like Firefox.
nnoremap <leader><tab> :tabprevious<CR>
nnoremap <leader><tab> :tabnext<CR>
nnoremap <leader>t     :tabnew<CR>

" Map Ctrl-n to open NERD Tree
nnoremap <C-n> :NERDTreeToggle<CR>

" Pressing <Enter> in normal mode will save if buffer has been modified.
nnoremap <silent> <expr> <CR> &modified ? ':w<CR>' : '<CR>'

" Open tag in new tab <leader><C-]>
nnoremap <leader><C-]> <C-w><C-]><C-w>T

" Toggle spell check mapped to <leader>s
nnoremap <leader>s :set spell!<CR>

" Toggle paste mode mapped to <leader>p
nnoremap <leader>p :set paste!<CR>

" Remove search highlight
nnoremap <leader>/ :noh<CR>

" Map j, k to gj, gk for long wrapped lines
nnoremap j gj
nnoremap k gk

" FZF keybinds
nnoremap <leader>fo :Buffers<CR>
nnoremap <leader>ff :Files<CR>

" Grepper keybinds
nnoremap <leader>sg :Grepper<CR>

" Git keybinds
nnoremap <leader>b :Git blame<CR>

" Autoformat keybind, verbose mode on
noremap <F3> :Autoformat<CR>
let g:autoformat_verbosemode=1

" Track session with Obession
nnoremap <leader>S :Obsession Session.vim<CR>
nnoremap <leader>R :Obsession!<CR>

" Map Y to copy to system clipboard
nnoremap Y "+y
vnoremap Y "+y

" Define Caw (close all windows) to close the quickfix and location list
" windows.
command! Caw :ccl | :lcl
