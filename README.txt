This repo relies on chezmoi to install and manage the dotfiles. The following
directories rely on syncthing to sync this directory with other machines:
  * oh-my-zsh
  * tmux
  * vim
